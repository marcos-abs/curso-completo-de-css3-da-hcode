# box-sizing-with-border-box

A propriedade "box-sizing" por padrão tem o valor "content-box" que no caso de utilizar "padding" acarretará um acrescimo ou decrescimo no tamanho do elemento, conquanto que no caso do valor de "box-sizing" estar "border-box" o elemento será redimensionado para "caber" a(s) alteração(ões)

---

tags: #box-sizing , #content-box, #border-box

---

fonte: [padding-top.html](padding-top.html)

---

```css
* {
    box-sizing: content-box;
}
```

esse é o valor padrão

---

Veja o exemplo
